Vue.component('card', {
    props: ['user'],
    template: `
    <div class="card">
        <div class="card__avatar">
            <img class="card__avatar-image" v-bind:alt="user.name" v-bind:src="user.big_avatar">
        </div>
        <div class="card__content">
            <div class="card__title">{{ user.name }}</div>
            <p class="card__text">
                {{ user.description }}
            </p>
            <div class="card__small-text">
                <img class="card__small-avatar" v-bind:alt="user.name" v-bind:src="user.small_avatar">
                {{ user.small_text }}
            </div>
        </div>
        <div class="card__info">
            <div class="card__rate">{{ user.price }}¢</div>
            <div class="card__sales">{{ user.sales_percent }}%</div>
            <div class="card__rating">
                <span class="card__rating-positive">{{ user.rate_success }}</span>
                &nbsp;/&nbsp;
                <span class="card__rating-negative">{{ user.rate_fail }}</span>
            </div>
        </div>
    </div>`
})

var app = new Vue({
    el: '#app',
    data() {
        return {
            users: [],
            loading: false,
            page: 1
        };
    },
    template: `
    <div class="container">
        <card v-for="(user, index) in users" :key="index" v-bind:user="user"></card> 
        <button
            class="button-load-more"
            @click="loadMore()">
            {{ loading ? 'Loading...' : 'Load more' }}
        </button>
    </div>`,
    mounted() {
        this.getUsers();
        window.addEventListener('scroll', this.handleScroll.bind(this), );
    },
    methods: {
        getUsers() {
            this.loading = true;
            axios
                .get("http://infite.net/site/mock?page=" + this.page)
                .then(response => {
                    if (response.data.results.length) {
                        this.users = this.users.concat(response.data.results);
                    } else {
                        this.page--;
                    }
                })
                .catch(error => {
                    this.page--;
                    console.log(error);
                })
                .finally(() => {
                    this.loading = false;
                })
        },
        loadMore() {
            if (!this.loading) {
                this.page++;
                this.getUsers();
            }
        },
        handleScroll() {
            if (window.scrollY >= (document.body.scrollHeight - window.innerHeight) * 0.9) {
                this.loadMore();
            }
        }
    }
})